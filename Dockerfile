# Create a custom image based on alpine with python3
FROM telegraf:alpine

RUN apk update && apk upgrade && apk add python3 py3-urllib3 bash