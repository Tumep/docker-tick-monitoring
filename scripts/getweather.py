from datetime import datetime, timezone
from xml.dom import minidom
import urllib3

'''
raw temperature test url http://opendata.fmi.fi/wfs/fin?service=WFS&version=2.0.0&request=GetFeature&storedquery_id=fmi::observations::weather::timevaluepair&place=Katajanokka&parameters=temperature&timestep=10
raw forecast test url http://opendata.fmi.fi/wfs?service=WFS&version=2.0.0&request=GetFeature&storedquery_id=fmi::forecast::hirlam::surface::point::timevaluepair&place=Kaisaniemi&timestep=60&parameters=temperature
'''

def datetime_to_timestamp_ns(raw_date):
  timestamp = datetime.strptime(raw_date, "%Y-%m-%dT%H:%M:%SZ")
  time_ns = int(datetime.replace(timestamp, tzinfo=timezone.utc).timestamp()) * 1000000000
  return time_ns

def get_weather(url, timetype):
  http = urllib3.PoolManager()
  r = http.request('GET', url)
  mydoc = minidom.parseString(r.data)
  items = mydoc.getElementsByTagName('wml2:MeasurementTVP')
  location = "Kaisaniemi"
  name = "weather"
  variable = timetype
  for elem in items:
    temperature = float(elem.childNodes[3].firstChild.data)
    time_ns = datetime_to_timestamp_ns(elem.childNodes[1].firstChild.data)
    message = "%s,location=%s %s=%.1f %d\n" % (name, location, variable, temperature, time_ns)
    print(message)
  http.clear()



def main():
  urls = ["http://opendata.fmi.fi/wfs/fin?service=WFS&version=2.0.0&request=GetFeature&storedquery_id=fmi::observations::weather::timevaluepair&place=Katajanokka&parameters=temperature&timestep=30", 
          "http://opendata.fmi.fi/wfs?service=WFS&version=2.0.0&request=GetFeature&storedquery_id=fmi::forecast::hirlam::surface::point::timevaluepair&place=Kaisaniemi&timestep=60&parameters=temperature"]
  get_weather(urls[0], "temperature")
  get_weather(urls[1], "forecast")

if __name__ == '__main__':
  main()