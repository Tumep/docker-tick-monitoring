from datetime import datetime, timezone
from xml.dom import minidom
import urllib3
import json

def get_bikes():
  url = "https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql"
  http = urllib3.PoolManager()
  data = """{ Narinkka:bikeRentalStation(id: "025") {
            name
            bikesAvailable
            spacesAvailable
            allowDropoff
          }
          Kamppi:bikeRentalStation(id: "026"){
            name
            bikesAvailable
            spacesAvailable
            allowDropoff
          }
          Kanavanranta:bikeRentalStation(id: "012"){
            name
            bikesAvailable
            spacesAvailable
            allowDropoff
          }          
        }"""
  r = http.request('POST', url, headers={ "Content-Type": "application/graphql"}, body=data)

  # Csv parsing due to json object notation not parsing correctly in influx json parser (flattener)
  # First create header and insert data after
  csv_header = "measurement,"
  csv_data = ""
  result = r.data.decode('utf8')
  jsondata = json.loads(result)
  for i in jsondata['data']['Narinkka'].keys():
    csv_header += i + ','
  csv_header = csv_header[:-1]

  for key in jsondata['data'].keys():
    csv_data += "bikes," + jsondata['data'].get(key)['name'] + "," + str(jsondata['data'].get(key)['bikesAvailable']) + "," + str(jsondata['data'].get(key)['spacesAvailable']) + "," + str(jsondata['data'].get(key)['allowDropoff']) + "\n"  
  print(csv_header, csv_data, sep='\n')

def main():
  get_bikes()

if __name__ == '__main__':
  main()